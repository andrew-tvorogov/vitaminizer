<?php

class Router
{

    private $routes;

    public function __construct()
    {
        include_once('config/routes.php');
        $this->routes = $routes;
    }

    public function run()
    {
        $requestedUrl = trim($_SERVER['REQUEST_URI'], '/');

        // если вошли в корень сайта или корень плюс index*   (.*)([^0-9])+$
        $patternIndex = SITE_ROOT . "ind(.*)([^0-9])+$";

        if ( $requestedUrl === trim(SITE_ROOT, '/') OR preg_match("~$patternIndex~", $requestedUrl) )
        {
            $requestedUrl = SITE_ROOT . 'main'; // переходим на главную страницу
        }

        $urlFound = false; // флаг - начальное значение -> url не найден
        foreach ($this->routes as $controller => $availableRoutes) {

            foreach ($availableRoutes as $availableRoute => $actionWithParameters) {

                if (preg_match("~$availableRoute~", $requestedUrl)) {
                    $actionWithParameters = preg_replace("~$availableRoute~", $actionWithParameters, $requestedUrl);
                    $actionWithParameters = str_replace(SITE_ROOT, '', $actionWithParameters);
                    $actionWithParametersArray = explode('/', $actionWithParameters);
                    $selectedController = new $controller();
                    $action = array_shift($actionWithParametersArray);
                    $selectedAction = 'action' . ucfirst($action);
                    call_user_func_array(array($selectedController, $selectedAction), $actionWithParametersArray);
                    $urlFound = true; // флаг - url найден!
                    break 2;
                }
            }
        }
        if ($urlFound === false) {
            header('Location: ' . FULL_SITE_ROOT . 'errors/404');
        }
    }
}