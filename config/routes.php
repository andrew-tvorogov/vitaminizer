<?php
$routes = array(

    'MainController' => array(
        'main' => 'index',
        'main/([0-9]+)' => 'index',
    ),

    'CaloriesController' => array(
        'calories' => 'index',
        'calories' => 'index',
    ),
    
    'UserController' => array(
        'users/reg' => 'register',
        'users/auth' => 'auth',
        'users/account/([0-9]+)' => 'account/$1',
        'users/edit/([0-9]+)' => 'edit/$1',
        'users/logout' => 'logout',
    ),

    'ErrorController' => array(
        'errors/([0-9]+)' => 'index/$1'
    )   
);