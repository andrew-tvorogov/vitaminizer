<?php
include_once('models/Calories.php');

class CaloriesController
{
    private $caloriesModel;

    public function __construct()
    {
        $this->caloriesModel = new Calories();
        $this->userModel = new User();
    }

    public function actionIndex()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $title = 'Calories';
        include_once('views/templates/header.php');
        include_once('views/pages/calories.php');
        include_once('views/templates/footer.php');
    }

}