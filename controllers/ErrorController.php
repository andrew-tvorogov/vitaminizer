<?php

class errorController
{

    private $userModel;

    public function __construct()
    {
        $this->userModel = new User();

    }

    public function actionIndex($errorNumber)
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $title = 'error!';


        $error = $errorNumber;


        include_once('views/error/index.php');
    }
}
