<?php
include_once('models/Main.php');

class MainController
{
    private $mainModel;
    private $userModel;

    public function __construct()
    {
        $this->mainModel = new Main();
        $this->userModel = new User();
    }

    public function actionIndex()
    {
        $title = 'Main';        

        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if (isset($_POST['email'])) {          
            $email = Helper::escape($_POST['email']);
            $password = Helper::escape($_POST['password']);

            $hash_password = md5($password);

                $userId = $this->userModel->getUserId($email, $hash_password);
                $user = $this->userModel->getUser($email, $hash_password);
                //print_r($user);
                $user_name = $user['user_name'];
                $admin = $user['user_is_admin'];

                setcookie('user', $userId, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('user_name', $user_name, time() + 2 * 24 * 3600, '/');
                setcookie('admin', $admin, time() + 2 * 24 * 3600, '/');

                $token = $this->userModel->generateToken();
                $tokenTime = time() + 15 * 60;
                setcookie('token', $token, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('tokenTime', $tokenTime, (time() + 2 * 24 * 60 * 60), '/');

                $this->userModel->addToken($userId, $token, $tokenTime);                            
                header('Location: ' . FULL_SITE_ROOT . 'main');
                //header('Location: .');
        }                       
        include_once('views/templates/header.php');
        include_once('views/pages/main.php');
        include_once('views/templates/footer.php');                                
    }

}
