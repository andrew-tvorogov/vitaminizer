<?php

class userController {
    private $userModel;
    private $gendersModel;


    public function __construct(){
        $this->userModel = new User();
        $this->mainModel = new Main();

        // $this->gendersModel = new Gender();
    }

    public function actionRegister()
    {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        if (isset($_POST['email'])) {
            $email = Helper::escape($_POST['email']);
            $name = Helper::escape($_POST['name']);
            $surname = Helper::escape($_POST['surname']);
            $password = Helper::escape($_POST['password']);
            $repeat_password = Helper::escape($_POST['repeat_password']);
            $emailExists = $this->userModel->checkIfEmailExists($email);
            $emailValidate = $this->userModel->emailValidate($email);
            $nameValidate = $this->userModel->nameValidate($name);
            $errors = [];

//            if ($email == '') {                     // проверка: поле EMAIL пустое или нет
//                $errors [] = 'Email not filled!';
//            }
//            if ($name == '') {                      // проверка: поле NAME пустое или нет
//                $errors [] = 'Name not filled!';
//            }
            if (!$emailValidate) {                  // проверка почты через регулярное выражение
                $errors ['01'] = 'Email entered incorrectly!';
            }
            if (!$nameValidate) {                   // проверка имени через регулярное выражение
                $errors ['02'] = 'Name entered incorrectly!';
            }
            if ($emailExists == 1) {                // проверка: создан email или нет
                $errors ['03'] = 'Email already exists!';
            }
            if ($password == '' || $repeat_password == '') { // проверка: поле Password пустое или нет
                $errors ['04'] = 'Password not filled!';
            }
            if ($password !== $repeat_password) {   // проверка: пароли совпадают или нет
                $errors ['05'] = 'Password mismatch!';
            }
            $password = md5($password);

//          print_r($errors);

            if (empty($errors)) {
                    $this->userModel->addNewUser($email, $password, $name, $surname);
                    $this->actionAuth();
                    $userId = $this->userModel->getUserId($email, $password);
                    header('Location: ' . FULL_SITE_ROOT . 'users/account/' . $userId);
            }
        } else {
            echo 'Error! Form clear!';
        }

        $title = 'Registration';
        include_once ('views/users/reg.php');
        return true;
    }

    public function actionAuth() {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();


        if(isset($_POST['email'])) {
            $email = Helper::escape($_POST['email']);
            $password = Helper::escape($_POST['password']);
//            echo 'email = ' . $email . '<br>';
//            echo 'password = ' . $password . '<br>';
//
            $hash_password = md5($password);
//            echo 'hash = ' . $hash_password . '<br>';
            $errors = [];
            // if (!$this->userModel->checkIfUserExists($email, $hash_password)) {
            //     $errors ["01"] = 'Email or password does not found!';
            // }
            // if ($email == '') {                     // проверка: поле EMAIL пустое или нет
            //     $errors ["03"] = 'Email not filled!';
            // }
            // if ($password == '') {
            //     $errors ["02"] = 'Password not filled!'; // проверка: поле PASSWORD пустое или нет
            // }


            if (empty($errors)) {
                $userId = $this->userModel->getUserId($email, $hash_password);
                $user = $this->userModel->getUser($email, $hash_password);
                // echo $userId;
                $user_name = $user['user_name'];
                $admin = $user['user_is_admin'];

                setcookie('user', $userId, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('user_name', $user_name, time() + 2 * 24 * 3600, '/');
                setcookie('admin', $admin, time() + 2 * 24 * 3600, '/');

                $token = $this->userModel->generateToken();
                $tokenTime = time() + 15 * 60;
                setcookie('token', $token, (time() + 2 * 24 * 60 * 60), '/');
                setcookie('tokenTime', $tokenTime, (time() + 2 * 24 * 60 * 60), '/');

                $this->userModel->addToken($userId, $token, $tokenTime);

                header('Location: ' . FULL_SITE_ROOT . 'users/account/' . $userId);
            }
        }
        // $title = 'Authorization';
        include_once ('views/users/auth.php');
        return true;
    }

    public function actionAccount($id) {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $title = 'Account';

        $user = $this->userModel->getAccountById($id);

//убираем возможность зайти в личный кабинет пользователя через командную строку
            if ($id === $_COOKIE['user']) {
                include_once ('views/users/account.php');
            } else {
                header('Location: ' . FULL_SITE_ROOT . 'errors/401');
            }
//      echo 'Вызван метод actionAccount';

    }

    public function actionEdit($id) {
        $isAuthorized = $this->userModel->checkIfAuthorized();
        $isAdmin = $this->userModel->checkIfAdmin();

        $user = $this->userModel->getAccountById($id);
        $genders = $this->gendersModel->getAll();


        $title = "Edit account";
//            if (empty($user)) header('Location: ' . FULL_SITE_ROOT . 'errors/2');
        if (isset($_POST['name'])) {
            $name = Helper::escape($_POST['name']);
            $surname = Helper::escape($_POST['surname']);
            $phone = Helper::escape($_POST['phone']);
            $address = Helper::escape($_POST['address']);
            $genderId = $_POST['genders'];

//                ПРОВЕРКА ЧЕРЕЗ РЕГУЛЯРНЫЕ ВЫРАЖЕНИЯ
            $nameValidate = $this->userModel->nameValidate($name);
            $surnameValidate = $this->userModel->surnameValidate($surname);
            $phoneValidate = $this->userModel->phoneValidate($phone);
            $addressValidate = $this->userModel->addressValidate($address);
            $errors = [];

            if (!$nameValidate) {                   // проверка имени через регулярное выражение
                $errors ['01'] = 'Name entered incorrectly!';
            }
            if (!$surnameValidate) {                 // проверка фамилии через регулярное выражение
                $errors ['02'] = 'Surname entered incorrectly!';
            }
            if (!$phoneValidate) {                   // проверка телефона через регулярное выражение
                $errors ['03'] = 'Phone entered incorrectly!';
            }
            if (!$addressValidate) {                   // проверка адреса через регулярное выражение
                $errors ['04'] = 'Address entered incorrectly!';
            }
            if (empty($errors)) {  // если массив с ошибками пустой, то выводим данные в бд через модель editUser
                $data = array(
                    'id' => $id,
                    'name' => $name,
                    'surname' => $surname,
                    'phone' => $phone,
                    'address' => $address,
                    'genderId' => $genderId
                );
                $this->userModel->editUser($data);
                header('Location: ' . FULL_SITE_ROOT . 'users/account/' . $id);
            }
        }
//        echo 'Users:';
//        echo '<pre>';
//        print_r ($user);
//        echo '</pre>';
//        echo '<br>';

//        echo 'POST:';
//        echo '<pre>';
//        print_r ($_POST);
//        echo '</pre>';
//        echo '<br>';

//        echo 'Genders:';
//        echo '<pre>';
//        print_r ($genders);
//        echo '</pre>';

//        echo 'Вызван метод editAccount в UserController';
        include_once ('views/users/edit_account.php');

    }

    public function actionLogout()
    {
        if (isset($_COOKIE['user'])) {
            setcookie('user', '', (time() - 3600), '/');
            setcookie('token', '', (time() - 3600), '/');
            setcookie('tokenTime', '', (time() - 3600), '/');
            setcookie('cart', '', (time() - 3600), '/');

            setcookie('key', '', (time() - 3600), '/');
            setcookie('user_name', '', (time() - 3600), '/');
            setcookie('admin', '', (time() - 3600), '/');

            header('Location: ' . FULL_SITE_ROOT);
        }
    }
}