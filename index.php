<?php

//подключаем пути
include_once ('components/autoload.php');
include_once ('config/constants.php');

//создаем экземпляр класса router
$router = new Router ();

//вызываем метод run в router
$router->run();

?>