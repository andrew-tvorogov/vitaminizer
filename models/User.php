<?php

class User
{
    private $db;

    public function __construct()
    {
        include('config/db.php');
        $this->db = DB::getInstance();
    }

    public function checkIfEmailExists($email)
    {
        $query = "
        SELECT COUNT(*) as `count`
        FROM `users`
        WHERE `user_email` = '$email';
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
//        echo 'count = ' . $count . '<br>';
        return $count;
    }

    public function checkIfUserExists($email, $password){
        $query = "
        SELECT COUNT(*) AS `count`
        FROM `users`
        WHERE `user_email` = '$email' AND `user_password` = '$password';
        ";
        $result = mysqli_query($this->db, $query);
        $count = mysqli_fetch_assoc($result)['count'];
        echo 'COUNT = ' . $count;
        return $count;
    }

    public function getUserId($email, $password)
    {
        $query = "SELECT * 
                  FROM `users` 
                  WHERE `user_email` = '$email' AND `user_password` = '$password';
                        ";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result)['user_id'];
    }
    public function getAll(){
        $query = "
					SELECT *
					FROM `users`
				";
        $user = mysqli_query($this->db, $query);
        return mysqli_fetch_all($user, MYSQLI_ASSOC); // Получаем массив из бд
    }

    public function getCountUsers() {
        $query = "
					  SELECT COUNT(*) AS `count`
                      FROM `users`
				";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }
    public function getCountMale() {
        $query = "
					 SELECT COUNT(`user_gender_id`) AS `male` FROM `users` WHERE `user_gender_id` = 1
				";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }
    public function getCountFemale() {
        $query = "
					 SELECT COUNT(`user_gender_id`) AS `female` FROM `users` WHERE `user_gender_id` = 2
				";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    public function getUser ($email, $password){
        $query = "
					SELECT *
					FROM `users`
					WHERE `user_email` = '$email'
					AND `user_password` = '$password';
				";
        $result = mysqli_query($this->db, $query);
        return mysqli_fetch_assoc($result);
    }

    public function getAccountById($id)
    {
//        echo 'внутри функции getAccountById <br> id пользователя = ' . $id;
        $query = " 
        SELECT *
		FROM `users`
		LEFT JOIN `genders` ON `gender_id` = `user_gender_id`
		WHERE `user_id` = '$id';
		";
        $user = mysqli_query($this->db, $query);
        return mysqli_fetch_all($user, MYSQLI_ASSOC); // Получаем массив из бд
    }

//    Делаем метку на пользователе, который будет удален
    public function deleteUser($id)
    {
//echo 'внутри функции deleteUser <br> id пользователя = ' . $id;

        $query = "
				UPDATE `users`
				SET `user_is_deleted` = 1
				WHERE `user_id` = $id;
			";
        mysqli_query($this->db, $query);
        return true;
    }

//    Удаляем пользователя из базы данных
    public function deleteUsersFromDB()
    {
        $query = "
               DELETE FROM `users`
               WHERE `user_is_deleted` = 1;
        ";
        mysqli_query($this->db, $query);
        return true;
    }



// редактируем пользователя в личном кабинете
    public function editUser($user)
    {
//        echo 'внутри функции editUser';
//        echo '<pre>';
//        print_r ($user);
//        echo '</pre>';

        $query = "
                    UPDATE `users` SET
                    `user_name` = '$user[name]',
                    `user_surname` = '$user[surname]',
                    `user_phone` = '$user[phone]',
                    `user_address` = '$user[address]',
                    `user_gender_id` = '$user[genderId]'
                    WHERE `user_id` = '$user[id]';
                    ";
        mysqli_query($this->db, $query);
        return true;
    }

// проверка авторизовано пользователь или нет
    public function checkIfAuthorized()
    {
        $isAuthorized = false;
        if (isset($_COOKIE['user']) && isset($_COOKIE['token']) && isset($_COOKIE['tokenTime'])) {
            $user_id = $_COOKIE['user'];
            $token = $_COOKIE['token'];
            $query = "
				SELECT `connect_id`, UNIX_TIMESTAMP(`connect_token_time`) AS `token_time`
				FROM `connects`
				WHERE `connect_user_id` = '$user_id'
				AND `connect_token` = '$token';
			";
            $result = mysqli_query($this->db, $query);
            $connect_info = mysqli_fetch_assoc($result);
            if ($connect_info) {
                $isAuthorized = true;
                if (time() > $connect_info['token_time']) {
                    $new_token = $this->generateToken();
                    $new_token_time = time() + 2 * 60;
                    $connect_id = $connect_info['connect_id'];
                    $query = "
						UPDATE `connects`
						SET `connect_token` = '$new_token',
							`connect_token_time` = FROM_UNIXTIME($new_token_time)
						WHERE `connect_id` = $connect_id;
					";
                    mysqli_query($this->db, $query);
                    setcookie('token', $new_token, time() + 2 * 24 * 3600, '/');
                }
            }
        }
        return $isAuthorized;
    }

// проверка администратор или нет
    public static function checkIfAdmin() {
        $isAdmin = false;
        if (isset($_COOKIE['user']) && isset($_COOKIE['token']) && isset($_COOKIE['tokenTime'])) {
            $userId = $_COOKIE['user']; // userId из cookie
            include('config/db.php');
            $connection = mysqli_connect($db['host'], $db['user'], $db['password'], $db['db_name']);
            mysqli_set_charset($connection, 'utf8');
            $query = "SELECT *
                        FROM `users`
                       WHERE `user_is_admin` = 1 
                         AND `user_id` = '$userId';
                   ";
            $result = mysqli_query($connection, $query);
            $userIsAdminFlagExist =  mysqli_fetch_assoc($result);
            if ($userIsAdminFlagExist) {
                $isAdmin = true;
            }
        }
        return $isAdmin;
    }

    public function generateToken($size = 32){
        $symbols = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'];
        $symbolsLength = count($symbols);
        $token = '';
        for ($i = 0; $i < $size; $i++){
            $token .= $symbols[rand(0, $symbolsLength - 1)];
        }
        return $token;
    }

    public function addToken ($userId, $token, $tokenTime){
        session_start();
        $session_id = session_id();
        $query = "INSERT INTO `connects`
                  SET `connect_user_id` ='$userId',
                      `connect_token` = '$token',
                      `connect_session_id` = '$session_id',
                      `connect_token_time` = FROM_UNIXTIME($tokenTime);
                        ";
        mysqli_query($this->db, $query);
        return true;
    }

// добавляем нового пользователя в базу данных из формы регистрации
    public function addNewUser($email, $password, $name, $surname){
//      echo 'add to db next user = ' . $name. '<br>email = ' . $email;

        $query = "INSERT INTO `users`
                  SET `user_email` = '$email',
                    `user_name` = '$name',
                    `user_surname` = '$surname',
                    `user_password` = '$password';
        ";
        mysqli_query($this->db, $query);
        return true;
    }

//  Проверка форм по регулярным выражениям
    public function emailValidate($email)
    {
        $pattern = '/^[0-9a-z]([0-9a-z_\-])*@((?1)|[0-9а-я])*\.((?1)|[0-9а-я]){2,10}$/iu';
        return preg_match($pattern, $email);
    }
    public function nameValidate($string)
    {
        $pattern = '/^[a-zA-Z\d\-\ ]{1,50}$/u';
        return preg_match($pattern, $string);
    }
    public function surnameValidate($string)
    {
        $pattern = '/^$|^[a-zA-Z\d\-]{1,50}$/u';
        return preg_match($pattern, $string);
    }
    public function phoneValidate($string)
    {
        $pattern = '/^$|^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/';
        return preg_match($pattern, $string);
    }
    public function addressValidate($string)
    {
        $pattern = '/^$|^[\w\ \"\'\-\,\.\:\;\(\)\!\?]{2,100}$/iu';
        return preg_match($pattern, $string);
    }
    public function numberValidate($string)
    {
        $pattern = '#^[0-9]+$#';
        return preg_match($pattern, $string);
    }
}