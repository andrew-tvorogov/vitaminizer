<?php
$errcode = array(
    '1' => 'ID dont found',
    '2' => 'ID dont found',
    '3' => 'ID dont found',
    '401' => 'You do not have permission to access content',
    '404' => 'The requested resource does not exist!'
);
$title = "Error!";
?>

<?php include_once('views/templates/header.php'); ?>

    <div class="container">
        <div class="container_error">
            <div class="">Ooops! We have a some problem!</div>
            <div class="" style="color: brown"> Code error: <?= $error; ?></div>
            <div class=""> <?= $errcode[$error]; ?>!</div>
        </div>
    </div>

<?php include_once('views/templates/footer.php'); ?>