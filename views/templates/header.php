<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="author" content="Ivan Porfirov">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title><?= $title ?></title>
    <!-- CSS -->
    <link rel="stylesheet" href="<?= CSS . 'font-awesome-4.7.0/css/font-awesome.min.css'; ?>">
    <link rel="stylesheet" href="<?= CSS . 'bootstrap-5.0.2/css/bootstrap.min.css'; ?>">

</head>

<body>
    <div class="container-fluid">
        <div class="row flex-nowrap">
            <div class="col-auto col-md-3 col-xl-2 px-sm-2 px-0 bg-dark">
                <div class="d-flex flex-column align-items-center align-items-sm-start px-3 pt-2 text-white min-vh-100">

                    <a href="<?= FULL_SITE_ROOT . 'main'; ?>" class="d-flex align-items-center pb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                        <span class="fs-5 mt-3 d-none d-sm-inline"><i class="fa fa-cutlery me-3"></i>VITAMINIZER</span>
                    </a>

                    <!-- * меню слева  -->
                    <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
                        <li class="nav-item ">
                            <a href="<?= FULL_SITE_ROOT . 'main'; ?>" class="nav-link align-middle px-0">
                                <span class="ms-1 d-none d-sm-inline text-success"><i class="fa fa-home me-2"></i>Overview</span>
                            </a>
                        </li>
                        <li>
                            <a href="#submenu1" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline text-success"><i class="fa fa-line-chart me-2"></i>Measurements</span><i class="text-success fa fa-angle-down ms-1"></i></a>
                            <ul class="collapse nav flex-column ms-1" id="submenu1" data-bs-parent="#menu">
                                <!-- * для отражения подменю всегда, необходимо указать в классе - show -->
                                <li class="w-100">
                                    <a href="<?= FULL_SITE_ROOT . 'calories'; ?>" class="nav-link px-0 text-success"> <span class="d-none d-sm-inline">Calories</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav-link px-0 text-success"> <span class="d-none d-sm-inline">Weight</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#submenu2" data-bs-toggle="collapse" class="nav-link px-0 align-middle">
                                <span class="ms-1 d-none d-sm-inline text-success"><i class="fa fa-calendar-check-o me-2"></i>Meal planner</span> <i class="text-success fa fa-angle-down ms-1"></i>
                            </a>
                            <ul class="collapse nav flex-column ms-1" id="submenu2" data-bs-parent="#menu">
                                <li class="w-100">
                                    <a href="#" class="nav-link px-0 text-success"> <span class="d-none d-sm-inline">meal</span> 1
                                    </a>
                                </li>
                                <li>
                                    <a href="#" class="nav-link px-0 text-success"> <span class="d-none d-sm-inline">meal</span> 2
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                    <hr>

                    <!-- 
                        ***
                            ЛИЧНЫЙ КАБИНЕТ 
                        *** 
                    -->
                    
                    <? if ($isAuthorized) : ?>
                        <div class="dropdown pb-4">
                            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="https://avatars.githubusercontent.com/u/3012" alt="hugenerd" width="30" height="30" class="rounded-circle">                                
                                <span class="d-none d-sm-inline mx-1">&nbsp;<?= $_COOKIE['user_name'] ?></span>                                
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark text-small shadow">
                                <li><a class="dropdown-item" href="#">Settings</a></li>
                                <li><a class="dropdown-item" href="#">Profile</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li><a class="dropdown-item" href="<?= FULL_SITE_ROOT . 'users/logout' ?>">Sign out</a></li>
                            </ul>
                        </div>
                    <? else : ?>
                        <div class="dropdown pb-4">
                            <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle" id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                                <img src="https://cdn4.iconfinder.com/data/icons/mayssam/512/add_user-512.png" alt="hugenerd" width="30" height="30" class="rounded-circle">
                                <span class="d-none d-sm-inline mx-1">&nbsp;&nbsp;Account</span>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-dark text-small shadow">
                                <li><a class="dropdown-item" href="#"><i class="fa fa-plus fas"></i>&nbsp;Create account</a></li>
                                <li>
                                    <hr class="dropdown-divider">
                                </li>
                                <li>
                                    <!-- кнопка для модального окна авторизации/регистрации-->
                                    <button type="button" class="btn btn-success mx-auto d-block" data-bs-toggle="modal" data-bs-target="#modalForm">Sign in</button>
                                </li>
                            </ul>
                        </div>

                        <!-- Модальное окно -->
                        <div class="modal fade" id="modalForm" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content text-dark">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Welcome back to <span class="text-success">Vitaminizer</span></h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">

                                        <form method="POST"> 
                                            <div class="mb-3">
                                                <label class="form-label">Email Address</label>
                                                <input type="text" class="form-control" id="email" name="email" placeholder="Username" />
                                            </div>
                                            <div class="mb-3">
                                                <label class="form-label">Password</label>
                                                <input type="password" class="form-control" id="password-input" name="password" placeholder="Password" />
                                            </div>
                                            <div class="mb-3 form-check">
                                                <input type="checkbox" class="form-check-input" id="rememberMe" />
                                                <label class="form-check-label" for="rememberMe">Remember me</label>
                                            </div>
                                            <div class="modal-footer d-block">
                                                <p class="float-start">Don't have an account? <a href="#" class="text-success">Sign Up</a></p>
                                                <button type="submit" class="btn btn-warning float-end">Submit</button>
                                            </div>
                                        </form>
                                    </div>

                                </div>
                            </div>
                        </div>
                    <? endif; ?>


                </div>
            </div>