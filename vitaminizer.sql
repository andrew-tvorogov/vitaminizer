-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Сен 14 2021 г., 14:44
-- Версия сервера: 10.4.16-MariaDB
-- Версия PHP: 7.3.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `vitaminizer`
--

-- --------------------------------------------------------

--
-- Структура таблицы `connects`
--

CREATE TABLE `connects` (
  `connect_id` int(10) UNSIGNED NOT NULL,
  `connect_user_id` int(10) UNSIGNED NOT NULL COMMENT 'ссылка на id пользователя',
  `connect_token` char(32) NOT NULL COMMENT 'создание токена для сессии',
  `connect_token_time` timestamp NOT NULL DEFAULT current_timestamp() COMMENT 'время создания сессии авторизованного пользователя',
  `connect_session_id` char(64) NOT NULL COMMENT 'id самой сессии'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `connects`
--

INSERT INTO `connects` (`connect_id`, `connect_user_id`, `connect_token`, `connect_token_time`, `connect_session_id`) VALUES
(54, 1, '097a464cc4b163d95159773443684499', '2020-12-04 11:16:59', '9jt3ktu61k4a2jvcecb5f2duja'),
(87, 1, 'A9D7833F8B68CBB89E4818E051062132', '2021-09-14 11:59:39', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(88, 1, '473A24386348E8A0A7B0660EBBAB671F', '2021-09-14 12:00:00', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(89, 1, '60B591719015EC574B1A0EADFA688346', '2021-09-14 12:00:01', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(90, 1, '5452ACE51789D4C4AB902425198DB22A', '2021-09-14 12:00:02', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(91, 1, '6BD5F117EF24378B5585CABFE5EA032A', '2021-09-14 12:00:02', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(92, 1, '2D68C2A97D2B40E6B5A75208678F1117', '2021-09-14 12:00:22', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(93, 1, '927482631C58AB0A1F55B218E74EA663', '2021-09-14 12:01:13', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(94, 1, 'F216BC30CEF69E45CE79A20134738031', '2021-09-14 12:03:12', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(95, 1, 'F61CCA6949D18C3F64E9B91641EDD49F', '2021-09-14 12:06:04', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(96, 1, '4C22334DE9BD2041A644398BAF2EE8CD', '2021-09-14 12:07:30', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(97, 1, 'D9909FE79A75BEC4F788360A5CD158AD', '2021-09-14 12:08:06', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(98, 1, 'D7F93B7587D3367F1418B6A43AAADCC4', '2021-09-14 12:08:15', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(99, 1, 'C8D1A5DBFCAD7D4F6EA0D40A57E4FE22', '2021-09-14 12:09:38', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(100, 1, '7CAD05010F7B9F3834A295CC83EB8496', '2021-09-14 12:10:28', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(101, 1, '9DB1203A577A5446DB37A8356CE869C8', '2021-09-14 12:11:03', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(102, 1, '9B3887E265AD4C424F40BD286182839B', '2021-09-14 12:13:37', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(103, 1, 'BB803B0B162408E232CC9A01EC93FE84', '2021-09-14 12:19:03', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(104, 1, 'B8B30C8106F1F7F03166A450F9A1F5D9', '2021-09-14 12:22:40', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(105, 1, 'A9231F6A5261D8EA2DEB8C17218DACA9', '2021-09-14 12:26:25', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(106, 1, '28D9C6D9444D6113C7C722C2B5956888', '2021-09-14 12:27:28', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(107, 1, '05D40090D1EE9783E04F2E7BCD23C326', '2021-09-14 12:27:44', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(108, 1, '4212E23C2F20E1D1897E85867CAEE138', '2021-09-14 12:28:02', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(109, 1, 'B00E034764EA6C91EB13B67F59852FD1', '2021-09-14 12:28:28', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(110, 1, '5315F6E7B1D2C45BD06F30D933A1382D', '2021-09-14 12:28:37', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(111, 1, '311DD3B28E351E41E052507A34BBA84C', '2021-09-14 12:38:51', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(112, 1, '5F76704A3377FBF78F2773AC520CFC23', '2021-09-14 12:40:22', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(113, 1, 'DF7474BD6B3173EB0CE642305080CE62', '2021-09-14 12:41:17', ''),
(114, 1, '2DAAC38809B5B4153A664828A37824A2', '2021-09-14 12:41:25', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(115, 1, '47FD5CB331FF89DD604A0C9E10CF71E8', '2021-09-14 12:44:46', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(116, 1, '3FF0B76E77ABD3674FBD0D730E2182B7', '2021-09-14 12:44:53', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(117, 1, 'AACF244EF2BD7FC0A878CAB4552F7490', '2021-09-14 12:50:45', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(118, 1, 'D6559BB7C388BB017BA34F7F7D3CA760', '2021-09-14 12:51:40', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(119, 1, '77420ABAE538BDFB250B113B9C51CED2', '2021-09-14 12:54:09', 'ce43ff8fdd22c7b78113a9bc4a1d22b1'),
(120, 1, '759F721630DF3347CD9105FD6D58D5B4', '2021-09-14 12:55:28', 'ce43ff8fdd22c7b78113a9bc4a1d22b1');

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `user_name` varchar(255) DEFAULT NULL COMMENT 'имя',
  `user_surname` varchar(255) DEFAULT NULL COMMENT 'фамилия',
  `user_email` varchar(255) DEFAULT NULL COMMENT 'логин',
  `user_password` char(32) DEFAULT NULL COMMENT 'пароль',
  `user_phone` varchar(255) DEFAULT NULL COMMENT 'телефон',
  `user_gender_id` tinyint(1) UNSIGNED DEFAULT NULL COMMENT 'пол',
  `user_address` varchar(255) DEFAULT NULL COMMENT 'адрес пользователя',
  `user_is_admin` tinyint(1) UNSIGNED DEFAULT 0 COMMENT 'админ или нет',
  `user_is_deleted` tinyint(1) UNSIGNED DEFAULT 0 COMMENT 'удален пользователь или нет'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`user_id`, `user_name`, `user_surname`, `user_email`, `user_password`, `user_phone`, `user_gender_id`, `user_address`, `user_is_admin`, `user_is_deleted`) VALUES
(1, 'Joanna', 'Hawk', 'user1@inbox.com', 'b59c67bf196a4758191e42f76670ceba', '111-11-11', 2, 'Abby Road 4', 0, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `connects`
--
ALTER TABLE `connects`
  ADD PRIMARY KEY (`connect_id`),
  ADD KEY `connect_user_id` (`connect_user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `connects`
--
ALTER TABLE `connects`
  MODIFY `connect_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `connects`
--
ALTER TABLE `connects`
  ADD CONSTRAINT `connects_ibfk_1` FOREIGN KEY (`connect_user_id`) REFERENCES `users` (`user_id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
